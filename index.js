var request = require("request");

function BunyanDiscord(options, error) {
    options = options || {};
	if (!options.webhook_url && !options.webhookUrl) {
		throw new Error('webhook url cannot be null');
	}
    this.customFormatter = options.customFormatter;
    this.webhook_url     = options.webhook_url || options.webhookUrl;
    this.error           = error               || function() {};
    this.message         = options.message     || "Hey @here - New log is available";
    this.avatar          = null;
    this.username        = null;

    if (options.botAvatarUrl || options.bot_avatar_url) {
        this.avatar = options.bot_avatar_url || options.botAvatarUrl;
    }
    if (options.botUsername || options.bot_username) {
        this.username = options.botUsername || options.bot_username;
    }
}

var nameFromLevel = {
	10: 'trace',
	20: 'debug',
	30: 'info',
	40: 'warn',
	50: 'error',
	60: 'fatal'
};

BunyanDiscord.prototype.addEmbed = function addEmbed(record) {
    var embedNode = {
        fields: [],
        title: "Log Report",
        timestamp: new Date().toISOString(),
    };
    var keys = Object.keys(record);
    keys.forEach((key) => {
        if(embedNode.fields.length > 24) {
            return;
        }
        var value = Reflect.get(record, key);
        if (key == "level") {
            var logName = Reflect.get(nameFromLevel, value);
            value = `${value} (${logName})`;
        }
        embedNode.fields.push({
            value,
            name: key,
            inline: true,
        });
    });
    return embedNode;
}

BunyanDiscord.prototype.write = function write(record) {
	var self = this, levelName, bodyContent;

    if (typeof record === 'string') {
		record = JSON.parse(record);
	}

    levelName = Reflect.get(nameFromLevel, record.level);
    bodyContent = {
        content: self.message,
        embeds: [],
    };
    if (self.username) {
        bodyContent.username = self.username;
    }
    if (self.avatar) {
        bodyContent.avatar_url = self.avatar;
    }

    bodyContent.embeds.push(self.addEmbed(record));
	request.post({
        headers: {
            "Content-Type": "application/json"
        },
		url: self.webhook_url,
		body: JSON.stringify(bodyContent)
	})
    .on('response', function(response) {
        var statusCode = response.statusCode;
        if(statusCode < 200 || statusCode >= 400) {
            return self.error({
                error: "",
                statusCode,
                body: response.toJSON(),
            });
        }
    })
	.on('error', function(err) {
		return self.error(err);
	});
};
module.exports = BunyanDiscord;
