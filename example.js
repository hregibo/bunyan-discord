const BunyanDiscord = require(".");
const bunyan = require("bunyan");

const discordReporter = new BunyanDiscord({
    botAvatarUrl: "https://i.imgur.com/2rHccsR.png",
    botUsername: "Magnificiant Bunyan Discord",
    webhookUrl: "https://discordapp.com/api/webhooks/your.webhook.id/your.webhook.token",
}, (err) => {
    console.error("Error: ", err);
});

const log = bunyan.createLogger({
    name: 'discordLogging',
    streams: [
        {stream: discordReporter, level: "error", type: "raw"},
    ],
});
log.error("Beep Boop i'm a bot!");