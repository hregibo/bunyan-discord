# Bunyan-discord

_ A simple port of the bunyan logging system to use Discord's Webhooks_

## Installation

Using NPM, you can install bunyan-discord by using this command:

    npm i -s @hregibo/bunyan-discord

or simply clone this project and require it in your project.

## Usage

Before declaring your Bunyan instance, create a new instance of bunyan-discord:

```javascript
const BunyanDiscord = require("@hregibo/bunyan-discord");
const discordReporter = new BunyanDiscord({
    // Your bot avatar, defaults to the one set on the Webhook
    botAvatarUrl: "https://i.imgur.com/2rHccsR.png",
    // Your bot name, defaults to the one set on the Webhook
    botUsername: "Log Reporter",
    // Eventually, customize the message, to include a @everyone for example
    // By default, the message is "Hey @here - New log is available"
    message: "Hey there @everyone ; looks like we failed somewhere!",
    // The webhook uri that you obtained when creating your discord webhook
    webhookUrl: "https://discordapp.com/api/webhooks/{webhook.id}/{webhook.token}",
}, (err) => {
    console.error("Error: ", err);
});
```

then, when creating your bunyan logger instance, just give the created variable as a stream
```javascript
const log = bunyan.createLogger({
    name: 'discordLogging',
    streams: [
        {stream: discordReporter, level: "error", type: "raw"},
    ],
});
```

and that's it ! Your logger now can send any log matching the level requirement to your discord channel!

If you execute our example (in example.js) you'd get something like this:

![img](https://i.imgur.com/7reWbTQ.png)


## Todo

* Add a typescript support, and a @types
* Allow full customization of the webhook, by adding any extra value given by the user

## Contributing

Simply do a merge request with the feature, and i'll do my best to check it and approve it hopefully!